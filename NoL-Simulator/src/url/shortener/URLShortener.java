/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.shortener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Ron
 */
public class URLShortener {

    final char[] ELEMENTS = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4',
        '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z'
    };
    final int[] KEY = {77105, 110100, 12686, 97108, 108101, 121126};    //Key to encode and decode URL
    final int[][] DECODED_ELEMENTS = new int[6][62];
    ArrayList<ShortURL> urlDB = new ArrayList<>();
    String lastCreatedLink = "";
    String lastCustomLink = "";
    File file = new File("shortURLDB.db");
    FileWriter fileWriter;
    BufferedReader fileReader;
    int numOfLink = 0;

    public URLShortener() throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        } else {
            String readLine = null;
            String[] strLine = null;
            fileReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            while ((readLine = fileReader.readLine()) != null) {
                strLine = readLine.split("[;]");
                urlDB.add(new ShortURL(strLine[1], strLine[0], (strLine[5].equalsIgnoreCase("yes"))));
            }
        }
        numOfLink = urlDB.size() - 1;
        //Initialize Encode and Decode elements
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 62; j++) {
                DECODED_ELEMENTS[i][j] = ((KEY[i] % 62) * (j + 1) % 62);
            }
        }
    }

    public String createShortURL(String url, String customURL, boolean pay2Click) {
        if (url.equals(lastCreatedLink) && customURL.equals(lastCustomLink)) {
            return "0";
        }
        lastCreatedLink = url;
        lastCustomLink = customURL;
        String shortURL = "http://srt.com/";
        if (customURL.isEmpty()) {
            numOfLink++;
            int seed = ((numOfLink - 1) / 62);
            int elementLocation;
            for (int i = 0; i < 6; i++) {
                elementLocation = (((DECODED_ELEMENTS[i][(numOfLink - 1) % 62]) + seed) % 62);
                shortURL = shortURL + ELEMENTS[elementLocation];
            }
        } else {
            shortURL = shortURL + customURL;
        }
        for (int i = 0; i < urlDB.size(); i++) {
            if (urlDB.get(i).getShortURL().equals(shortURL)) {
                return "-1";
            }
        }
        urlDB.add(new ShortURL(url, shortURL, pay2Click));
        return shortURL;
    }

    public int getURLNum(String shortURL) {
        for (int i = 0; i < urlDB.size(); i++) {
            if (urlDB.get(i).getShortURL().equals(shortURL)) {
                return i;
            }
        }
        return -1;
    }

    public int updateTime(int i) {
        if (urlDB.get(i).isDead()) {
            ShortURL temp = urlDB.get(i);
            urlDB.set(i, new ShortURL(temp.getRedirectURL(), temp.getShortURL(), temp.isPay2Click()));
        } else {
            urlDB.get(i).updateTimeDead();
        }
        return urlDB.get(i).getTimeDead();
    }

    public boolean clickURL(int i) {
        if (!urlDB.get(i).isDead()) {
            if (urlDB.get(i).isPay2Click()) {
                MainURL.totalPaidClick++;
            }
            urlDB.get(i).click();
            return true;
        } else {
            return false;
        }
    }

    public boolean getURLStatus(int i) {
        return !urlDB.get(i).isDead();
    }

    public String[] getURLData(int i) {
        return urlDB.get(i).toString().split("[;]");
    }

    public String getURLList() {
        String urlList = "";
        for (int i = 0; i < urlDB.size(); i++) {
            urlList += (urlDB.get(i).getShortURL() + "\n");
        }
        return urlList;
    }

    public synchronized void save2File() throws IOException {
        fileWriter = new FileWriter(file.getAbsoluteFile(), false);
        for (int i = 0; i < urlDB.size(); i++) {
            fileWriter.write(urlDB.get(i).getShortURL() + ";" + urlDB.get(i).toString() + "\n");
            fileWriter.flush();
        }
    }
}
