/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.shortener;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Ron
 */
public class FXMLURLShortenerController implements Initializable {

    @FXML
    private VBox urlShortenerPanel;
    @FXML
    private TextField longURLInput;
    @FXML
    private TextField customURLInput;
    @FXML
    private CheckBox pay2ClickInput;
    @FXML
    private Button shortenURLBtn;
    @FXML
    private Label longURLStatus;
    @FXML
    private TextField shortURLOutput;
    @FXML
    private VBox urlActionPanel;
    @FXML
    private VBox shortURLInfo;
    @FXML
    private TextField shortURLInput;
    @FXML
    private Button topUpBtn;
    @FXML
    private Button clickBtn;
    @FXML
    private Button getStatusBtn;
    @FXML
    private Button urlShortPanelBtn;
    @FXML
    private Button urlActionPanelBtn;
    @FXML
    private Label shortURLStatus;
    @FXML
    private TextArea shortURLTextArea;
    @FXML
    private TextArea urlList;
    @FXML
    private ToggleButton urlListBtn;
    @FXML
    private Text copiedLabel;

    boolean p2cFlag = false;    //Pay to Click Dialog flag
    Alert alert;
    FadeTransition ft;
    String currentURL = "";
    int currentURLNum = -1;     // '-1' means no URL

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    //Trigger to Display URL Shortener Panel
    @FXML
    private void urlShortPanelAction(ActionEvent event) {
        resetPanel();
        ft = new FadeTransition(Duration.millis(500), urlShortenerPanel);
        ft.setFromValue(0);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.play();
        urlShortenerPanel.setVisible(true);
        urlActionPanel.setVisible(false);
    }

    //Trigger to Display URL Action Panel
    @FXML
    private void urlActionPanelAction(ActionEvent event) {
        resetPanel();
        ft = new FadeTransition(Duration.millis(500), urlActionPanel);
        ft.setFromValue(0);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.play();
        urlShortenerPanel.setVisible(false);
        urlActionPanel.setVisible(true);
    }

    @FXML
    private void pay2ClickAction(ActionEvent event) {
        if (p2cFlag == false) {
            p2cFlag = true;
            alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Pay to click link will only active for 60 seconds\nTo increase the active time, \nclick the TopUp button at URL Action Panel");
            alert.showAndWait();
        }
    }

    @FXML
    private void shortenURLAction(ActionEvent event) {
        String shortURL;
        if (longURLInput.getText().length() < 15) {
            alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("URL too Short");
            alert.setContentText("Ooops, Your URL is shorter than my Shortened URL!");
            alert.showAndWait();
            return;
        }

        /*
         If return value '0' means no changes made compare to previous action
         If return value '-1' means URL already exist
         */
        if ((shortURL = MainURL.urlShortener.createShortURL(longURLInput.getText().replace(" ", ""), customURLInput.getText().replace(" ", ""), pay2ClickInput.isSelected())).equals("0")) {

        } else if (shortURL.equals("-1")) {
            longURLStatus.setText("ShortURL existed");
            longURLStatus.setTextFill(Paint.valueOf("RED"));
            shortURLOutput.setText("");
            ft = new FadeTransition(Duration.millis(1500), shortURLInfo);
            ft.setFromValue(0);
            ft.setToValue(1);
            ft.setCycleCount(1);
            ft.play();
        } else {
            longURLStatus.setText("Created");
            longURLStatus.setTextFill(Paint.valueOf("GREEN"));
            shortURLOutput.setText(shortURL);

            //Auto copy to clipboard
            StringSelection string = new StringSelection(shortURLOutput.getText());
            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
            clpbrd.setContents(string, null);

            ft = new FadeTransition(Duration.millis(1500), shortURLInfo);
            ft.setFromValue(0);
            ft.setToValue(1);
            ft.setCycleCount(1);
            ft.play();

            FadeTransition ft1 = new FadeTransition(Duration.millis(3000), copiedLabel);
            ft1.setFromValue(1);
            ft1.setToValue(0);
            ft1.setCycleCount(1);
            ft1.play();

            //Display pay 2 click dialog to remind user on the usage
            if (pay2ClickInput.isSelected()) {
                p2cFlag = false;
                pay2ClickAction(event);
            }
        }
    }

    @FXML
    private void topUpAction(ActionEvent event) {
        checkLink(event);
        if (currentURLNum != -1) {
            currentURL = shortURLInput.getText();
            int time = MainURL.urlShortener.updateTime(currentURLNum);
            shortURLStatus.setText("Topped Up");
            ft.play();
            String[] data = MainURL.urlShortener.getURLData(currentURLNum);
            shortURLTextArea.setText("URL \t\t\t: " + data[0] + "\nTime\t\t\t: " + time);
        } else {
            shortURLStatus.setText("Can't ReDirect");
            ft.play();
        }
    }

    @FXML
    private void clickAction(ActionEvent event) {
        checkLink(event);
        if (currentURLNum != -1) {
            currentURL = shortURLInput.getText();
            boolean flag = MainURL.urlShortener.clickURL(currentURLNum);
            String[] data = MainURL.urlShortener.getURLData(currentURLNum);
            if (flag) {
                shortURLStatus.setText("Clicked");
                ft.play();
            } else {
                shortURLStatus.setText("Link Dead");
                ft.play();
            }
            shortURLTextArea.setText("URL \t\t\t: " + data[0] + "\nLast Active \t: " + data[3] + "\nNum Of Click\t: " + data[5]);
        } else {
            shortURLStatus.setText("Can't ReDirect");
            ft.play();
        }
    }

    @FXML
    private void getStatusAction(ActionEvent event) {
        checkLink(event);
        if (currentURLNum != -1) {
            currentURL = shortURLInput.getText();
            String[] data = MainURL.urlShortener.getURLData(currentURLNum);
            shortURLStatus.setText("Get Status");
            ft.play();
            shortURLTextArea.setText("URL \t\t\t: " + data[0] + "\nStatus \t\t: " + data[1] + "\nTime Created \t: " + data[2] + "\nLast Active \t: " + data[3] + "\nPay to Click \t: " + data[4] + "\nNum Of Click\t: " + data[5]);
        } else {
            shortURLStatus.setText("Can't ReDirect");
            ft.play();
        }
    }

    @FXML
    private void urlListAction(ActionEvent event) {
        if (!urlList.isVisible()) {
            urlList.setVisible(true);
            urlList.setText(MainURL.urlShortener.getURLList());
        } else {
            urlList.setVisible(false);
        }
    }

    //To check ShortURL input
    public void checkLink(ActionEvent event) {
        ft = new FadeTransition(Duration.millis(750), shortURLStatus);
        ft.setFromValue(0);
        ft.setToValue(1);
        ft.setCycleCount(1);
        if (shortURLInput.getText().isEmpty()) {
            shortURLStatus.setText("No Link");
            return;
        }
        if (!shortURLInput.equals(currentURL)) {
            currentURLNum = MainURL.urlShortener.getURLNum(shortURLInput.getText());
        }
    }

    public void resetPanel() {
        longURLInput.setText("");
        customURLInput.setText("");
        pay2ClickInput.setSelected(false);
        shortURLInfo.setOpacity(0);

        shortURLInput.setText("");
        shortURLStatus.setOpacity(0);
        shortURLTextArea.setText("");

    }
}
