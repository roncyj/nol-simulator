/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.shortener;

import java.sql.Timestamp;

/**
 *
 * @author Ron
 */
public class ShortURL {

    private String redirectURL;
    private String shortURL;
    private String[] timeCreated;
    private int timeDead;
    private String lastActive;
    private boolean pay2Click;
    private int numOfClick = 0;

    public ShortURL(String redirectURL, String shortURL, boolean pay2Click) {
        this.redirectURL = redirectURL;
        this.shortURL = shortURL;
        this.pay2Click = pay2Click;
        timeCreated = (new Timestamp(new java.util.Date().getTime())).toString().substring(11, 19).split("[:]");
        if (pay2Click) {
            timeDead = 60;   //Default time in Seconds
        } else {
            timeDead = 300;  //Default time in Seconds
        }
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public String getShortURL() {
        return shortURL;
    }

    public int getTimeDead() {
        return timeDead;
    }

    public void updateTimeDead() {
        timeDead = timeDead + 15;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive() {
        lastActive = (new Timestamp(new java.util.Date().getTime())).toString().substring(11, 19);
    }

    public boolean isPay2Click() {
        return pay2Click;
    }

    public int getNumOfClick() {
        return numOfClick;
    }

    public void click() {
        numOfClick++;
        setLastActive();
    }

    public boolean isDead() {
        String[] current = (new Timestamp(new java.util.Date().getTime())).toString().substring(11, 19).split("[:]");

        //Idea of this code is use the Current time MINUS the Time Created
        int[] time = new int[3];
        time[0] = (Integer.valueOf(current[0]) - Integer.valueOf(timeCreated[0]));
        if (Integer.valueOf(current[1]) < Integer.valueOf(timeCreated[1])) {
            time[1] = (60 + Integer.valueOf(current[1]) - Integer.valueOf(timeCreated[1]));
            time[0]--;
        } else {
            time[1] = (Integer.valueOf(current[1]) - Integer.valueOf(timeCreated[1]));
        }
        if (Integer.valueOf(current[2]) < Integer.valueOf(timeCreated[2])) {
            time[2] = (60 + Integer.valueOf(current[2]) - Integer.valueOf(timeCreated[2]));
            time[1]--;
        } else {
            time[2] = (Integer.valueOf(current[2]) - Integer.valueOf(timeCreated[2]));
        }
        int seconds = (time[0] * 360) + (time[1] * 60) + time[2];
        return (timeDead < seconds);
    }

    public String toString() {
        return redirectURL + ";" + (!isDead() ? "Alive" : "Dead") + ";" + timeCreated[0] + ":" + timeCreated[1] + ":" + timeCreated[2] + ";" + lastActive + ";" + (pay2Click ? "Yes" : "No") + ";" + numOfClick;
    }
}
