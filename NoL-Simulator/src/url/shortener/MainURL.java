/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.shortener;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Ron
 */
public class MainURL extends Application {

    public static URLShortener urlShortener;
    public static ExecutorService exec;
    public static AutoSave autoSave;
    public static int totalPaidClick = 0;

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(initialize());
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        launch(args);
        beforeExit();
    }

    public Parent initialize() throws IOException {
        urlShortener = new URLShortener();
        autoSave = new AutoSave();
        exec = Executors.newCachedThreadPool();
        exec.submit(autoSave);
        Parent root = FXMLLoader.load(getClass().getResource("FXMLURLShortener.fxml"));
        return root;
    }

    public static void beforeExit() {
        autoSave.interrupt();
        exec.shutdown();
    }

    public static class AutoSave implements Runnable {

        public boolean isInterrupt = false;

        public void run() {
            while (!isInterrupt) {
                try {
                    Thread.sleep(3000);
                    MainURL.urlShortener.save2File();
                } catch (IOException ex) {
                    Logger.getLogger(MainURL.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainURL.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public void interrupt() {
            isInterrupt = true;
        }

    }

}
