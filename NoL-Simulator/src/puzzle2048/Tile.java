/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle2048;

import java.util.Random;
import javafx.scene.control.Label;

/**
 *
 * @author Ron
 */
public class Tile extends Label {

    private int value;
    private Coordinates coor;
    Label label;

    public Tile(Label label) {
        value = 0;
        this.label = label;
    }

    public void newTile() {
        //Set Tile value randomly with 90% of possibility getting value 2
        setValue(new Random().nextDouble() < 0.9 ? 2 : 4);
    }

    public void clearTile() {
        label.getStyleClass().remove("game-tile-" + value);
        setValue(0);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        label.getStyleClass().remove("game-tile-" + value);
        this.value = value;
        label.setText(Integer.toString(value));
        label.getStyleClass().add("game-tile-" + value);
    }

    public Coordinates getCoor() {
        return coor;
    }

    public void setCoor(Coordinates coor) {
        this.coor = coor;
    }

    public boolean isMergeable(Tile tile) {
        return (getValue() == tile.getValue());
    }

    public void merge(Tile tile) {
        label.getStyleClass().remove("game-tile-" + value);
        this.value += tile.getValue();
        label.setText(Integer.toString(value));
        label.getStyleClass().add("game-tile-" + value);
    }

    public Label getLabel() {
        return label;
    }
}
