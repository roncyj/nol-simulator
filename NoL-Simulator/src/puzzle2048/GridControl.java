/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle2048;

import java.util.Random;
import java.util.Vector;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;

/**
 *
 * @author Ron
 */
public class GridControl {

    FXML2048PuzController controller;
    Tile[][] gameGrid = new Tile[4][4];
    Vector<Coordinates> available;
    private final ParallelTransition parallelTransition = new ParallelTransition();
    public static int scores = 0;
    int bestScores = 0;
    public boolean messageFlag = false;

    public GridControl(FXML2048PuzController controller) {
        this.controller = controller;

        //Initialize gameGrid
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == 0) {
                    if (j == 0) {
                        gameGrid[i][j] = new Tile(controller.grid11);
                    } else if (j == 1) {
                        gameGrid[i][j] = new Tile(controller.grid12);
                    } else if (j == 2) {
                        gameGrid[i][j] = new Tile(controller.grid13);
                    } else if (j == 3) {
                        gameGrid[i][j] = new Tile(controller.grid14);
                    }
                } else if (i == 1) {
                    if (j == 0) {
                        gameGrid[i][j] = new Tile(controller.grid21);
                    } else if (j == 1) {
                        gameGrid[i][j] = new Tile(controller.grid22);
                    } else if (j == 2) {
                        gameGrid[i][j] = new Tile(controller.grid23);
                    } else if (j == 3) {
                        gameGrid[i][j] = new Tile(controller.grid24);
                    }
                } else if (i == 2) {
                    if (j == 0) {
                        gameGrid[i][j] = new Tile(controller.grid31);
                    } else if (j == 1) {
                        gameGrid[i][j] = new Tile(controller.grid32);
                    } else if (j == 2) {
                        gameGrid[i][j] = new Tile(controller.grid33);
                    } else if (j == 3) {
                        gameGrid[i][j] = new Tile(controller.grid34);
                    }
                } else if (i == 3) {
                    if (j == 0) {
                        gameGrid[i][j] = new Tile(controller.grid41);
                    } else if (j == 1) {
                        gameGrid[i][j] = new Tile(controller.grid42);
                    } else if (j == 2) {
                        gameGrid[i][j] = new Tile(controller.grid43);
                    } else if (j == 3) {
                        gameGrid[i][j] = new Tile(controller.grid44);
                    }
                }
            }
        }
        clearGame();
    }

    //Clear grid and reset Game
    public final void clearGame() {
        available = new Vector<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                gameGrid[i][j].clearTile();
                available.addElement(new Coordinates(i, j));
            }
        }
        scores = 0;
        updateScores(0);

        //Randomly generate 2 new Tile on grid
        newTile();
        newTile();
        updateScene();
        messageFlag = false;
    }

    public synchronized void newTile() {

        if (!available.isEmpty()) {
            int num = (new Random().nextInt(available.size()));
            gameGrid[available.elementAt(num).getX()][available.elementAt(num).getY()].newTile();
            available.removeElementAt(num);

        } else {
            System.out.println("Game Over");
        }
    }

    //Move coordinate c2 to c1;
    public synchronized void move(Coordinates c1, Coordinates c2) {
        gameGrid[c1.getX()][c1.getY()].setValue(gameGrid[c2.getX()][c2.getY()].getValue());
        gameGrid[c2.getX()][c2.getY()].clearTile();
        available.addElement(new Coordinates(c2.getX(), c2.getY()));
        for (int i = 0; i < available.size(); i++) {
            if ((available.elementAt(i).getX() == c1.getX()) && (available.elementAt(i).getY() == c1.getY())) {
                available.removeElementAt(i);
                break;
            }
        }
    }

    public synchronized void actionKey(KeyCode keyCode) {
        if (messageFlag) {
            return;
        }
        boolean flag = false;
        
        switch (keyCode.name()) {
            case "LEFT":
            case "RIGHT":
                for (int i = 0; i < 4; i++) {
                    int j;
                    if (keyCode.name().equalsIgnoreCase("LEFT")) {
                        j = 0;
                    } else {
                        j = 3;
                    }
                    for (; (keyCode.name().equalsIgnoreCase("LEFT") ? j < 4 : j >= 0);) {
                        int k;
                        if (keyCode.name().equalsIgnoreCase("LEFT")) {
                            k = j + 1;
                        } else {
                            k = j - 1;
                        }
                        for (; (keyCode.name().equalsIgnoreCase("LEFT") ? k < 4 : k >= 0);) {
                            if (gameGrid[i][k].getValue() != 0) {
                                if (gameGrid[i][j].getValue() == 0) {
                                    move(new Coordinates(i, j), new Coordinates(i, k));
                                    flag = true;
                                } else {
                                    if (gameGrid[i][j].isMergeable(gameGrid[i][k])) {
                                        gameGrid[i][j].merge(gameGrid[i][k]);
                                        updateScores(gameGrid[i][j].getValue());
                                        available.addElement(new Coordinates(i, k));
                                        gameGrid[i][k].clearTile();
                                        flag = true;
                                    }
                                    break;
                                }
                            }
                            if (keyCode.name().equalsIgnoreCase("LEFT")) {
                                k++;
                            } else {
                                k--;
                            }
                        }
                        if (keyCode.name().equalsIgnoreCase("LEFT")) {
                            j++;
                        } else {
                            j--;
                        }
                    }
                }
                break;

            case "UP":
            case "DOWN":
                for (int j = 0; j < 4; j++) {
                    int i;
                    if (keyCode.name().equalsIgnoreCase("UP")) {
                        i = 0;
                    } else {
                        i = 3;
                    }
                    for (; (keyCode.name().equalsIgnoreCase("UP") ? i < 4 : i >= 0);) {
                        int k;
                        if (keyCode.name().equalsIgnoreCase("UP")) {
                            k = i + 1;
                        } else {
                            k = i - 1;
                        }
                        for (; (keyCode.name().equalsIgnoreCase("UP") ? k < 4 : k >= 0);) {
                            if (gameGrid[k][j].getValue() != 0) {
                                if (gameGrid[i][j].getValue() == 0) {
                                    move(new Coordinates(i, j), new Coordinates(k, j));
                                    flag = true;
                                } else {
                                    if (gameGrid[i][j].isMergeable(gameGrid[k][j])) {
                                        gameGrid[i][j].merge(gameGrid[k][j]);
                                        updateScores(gameGrid[i][j].getValue());
                                        available.addElement(new Coordinates(k, j));
                                        gameGrid[k][j].clearTile();
                                        flag = true;
                                    }
                                    break;
                                }
                            }
                            if (keyCode.name().equalsIgnoreCase("UP")) {
                                k++;
                            } else {
                                k--;
                            }
                        }
                        if (keyCode.name().equalsIgnoreCase("UP")) {
                            i++;
                        } else {
                            i--;
                        }
                    }
                }
                break;
        }
        
        if ((available.size() == 0) && isGameOver()) {
            gameOver();
        }
        
        if (flag) {
            newTile();
            updateScene();
        }

    }

    private synchronized ScaleTransition addAnimation(Tile tile) {

        final ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100), tile.getLabel());
        if (tile.getValue() != 0) {
            scaleTransition.setFromX(0);
            scaleTransition.setFromY(0);
            scaleTransition.setToX(1.0);
            scaleTransition.setToY(1.0);
            scaleTransition.setInterpolator(Interpolator.EASE_OUT);
        } else {
            tile.getLabel().setScaleX(0);
            tile.getLabel().setScaleY(0);
        }
        return scaleTransition;
    }

    public synchronized void updateScene() {
        synchronized (gameGrid) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    parallelTransition.getChildren().add(addAnimation(gameGrid[i][j]));
                }
            }
            parallelTransition.play();
            parallelTransition.getChildren().clear();
        }
    }

    public boolean isGameOver() {
        boolean flag = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = j + 1; k < 4; k++) {
                    if (gameGrid[i][k].getValue() != 0) {
                        if (gameGrid[i][j].getValue() == 0) {
                            flag = false;
                        } else {
                            if (gameGrid[i][j].isMergeable(gameGrid[i][k])) {
                                flag = false;
                            }
                            break;
                        }
                    }
                }
            }
        }
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                for (int k = i + 1; k < 4; k++) {
                    if (gameGrid[k][j].getValue() != 0) {
                        if (gameGrid[i][j].getValue() == 0) {
                            flag = false;
                        } else {
                            if (gameGrid[i][j].isMergeable(gameGrid[k][j])) {
                                flag = false;
                            }
                            break;
                        }
                    }
                }
            }
        }
        return flag;
    }

    public void gameOver() {
        messageFlag = true;
        controller.failedDisplay.setVisible(true);
    }

    public void updateScores(int scores) {
        if (scores == 2048) {
            messageFlag = true;
            controller.winDisplay.setVisible(true);
        }
        this.scores += scores;
        if (this.scores >= bestScores) {
            bestScores = this.scores;
        }
        controller.updateScores(this.scores, bestScores);
    }
}
