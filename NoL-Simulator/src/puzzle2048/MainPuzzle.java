/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle2048;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 *
 * @author Ron
 */
public class MainPuzzle extends Application {

    FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML2048Puz.fxml"));
    public static FXML2048PuzController controller;
    public static GridControl gridControl;
    static File file;

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(initialize());
        //Keyboard key pressed listener
        scene.setOnKeyPressed(ke -> {
            KeyCode keyCode = ke.getCode();
            if (keyCode.isArrowKey()) {
                gridControl.actionKey(keyCode);
            }
        });
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        launch(args);
        beforeExit();

    }

    public Parent initialize() throws IOException {
        file = new File("puzzle2048Data.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        Parent root = loader.load();
        controller = loader.getController();
        gridControl = new GridControl(controller);
        
        //Read from file and initialize variable
        String readLine = null;
        String[] strLine = null;
        BufferedReader fileReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
        while ((readLine = fileReader.readLine()) != null) {
            if (!readLine.isEmpty()) {
                gridControl.bestScores = Integer.valueOf((readLine.split("[;]"))[0]);
            }
        }
        return root;
    }

    public static void beforeExit() throws IOException {
        //Save game Best Score to file
        try (FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), true)) {
            fileWriter.write("\n" + gridControl.bestScores + ";" + (new Timestamp(new java.util.Date().getTime())).toString().substring(0, 10));
            fileWriter.flush();
        }
    }
}
