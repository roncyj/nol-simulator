/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle2048;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Ron
 */
public class FXML2048PuzController implements Initializable {

    @FXML
    public Group gameGrid;
    @FXML
    public Label grid11;
    @FXML
    public Label grid12;
    @FXML
    public Label grid13;
    @FXML
    public Label grid14;
    @FXML
    public Label grid21;
    @FXML
    public Label grid22;
    @FXML
    public Label grid23;
    @FXML
    public Label grid24;
    @FXML
    public Label grid31;
    @FXML
    public Label grid32;
    @FXML
    public Label grid33;
    @FXML
    public Label grid34;
    @FXML
    public Label grid41;
    @FXML
    public Label grid42;
    @FXML
    public Label grid43;
    @FXML
    public Label grid44;
    @FXML
    public Group winDisplay;
    @FXML
    public Group failedDisplay;
    @FXML
    private Label puzzleScore;
    @FXML
    private Label puzzleBest;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void keepGoingAction(ActionEvent event) {
        MainPuzzle.gridControl.messageFlag = false;
        winDisplay.setVisible(false);
    }

    @FXML
    private void resetPuzzleAction(ActionEvent event) {
        failedDisplay.setVisible(false);
        winDisplay.setVisible(false);
        MainPuzzle.gridControl.clearGame();
    }

    public void updateScores(int scores, int bestScores) {
        puzzleScore.setText(String.valueOf(scores));
        puzzleBest.setText(String.valueOf(bestScores));

    }
}
