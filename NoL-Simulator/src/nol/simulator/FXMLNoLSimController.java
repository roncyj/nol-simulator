/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nol.simulator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Ron
 */
public class FXMLNoLSimController implements Initializable {

    @FXML
    public Pane simulatorPane;
    @FXML
    private Group navigatePane;
    @FXML
    public Label dayCount;
    @FXML
    public ToggleButton startBtn;
    @FXML
    public Label creditsCount;
    @FXML
    private Label additionalCredits;
    @FXML
    public Label energyValue;
    @FXML
    public ProgressBar energyBar;
    @FXML
    public Label hungerValue;
    @FXML
    public ProgressBar hungerBar;
    @FXML
    public Label moodValue;
    @FXML
    public ProgressBar moodBar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void startAction(ActionEvent event) {
        if (startBtn.isSelected()) {
            startBtn.setText("Pause");
            MainNoLSim.dayCounting.toggleDayCount(true);
        } else {
            startBtn.setText("Start");
            MainNoLSim.dayCounting.toggleDayCount(false);
        }
    }

    @FXML
    public void backAction(ActionEvent event) throws IOException {
        MainNoLSim.setCredit((MainNoLSim.mainPz.gridControl.scores) / 10);
        MainNoLSim.mainPz.gridControl.clearGame();
        MainNoLSim.setCredit(MainNoLSim.mainURL.totalPaidClick);
        MainNoLSim.mainURL.totalPaidClick = 0;
        simulatorPane.getChildren().clear();
        simulatorPane.getChildren().add(MainNoLSim.nolSimPane);
    }

    @FXML
    public void resetAllAction(ActionEvent event) {
        MainNoLSim.mainPz.gridControl.clearGame();
        MainNoLSim.mainURL.totalPaidClick = 0;
        simulatorPane.getChildren().clear();
        simulatorPane.getChildren().add(MainNoLSim.nolSimPane);
        MainNoLSim.dayCount =1;
        setDayCount(1);
        MainNoLSim.credits = 200;
        MainNoLSim.energy = 100;
        MainNoLSim.hunger = 100;
        MainNoLSim.mood = 100;
        MainNoLSim.setCredit(0);
        MainNoLSim.setEnergy(0);
        MainNoLSim.setHunger(0);
        MainNoLSim.setMood(0);
    }

    public void setDayCount(int value){
        dayCount.setText(String.valueOf(value));
    }
}
