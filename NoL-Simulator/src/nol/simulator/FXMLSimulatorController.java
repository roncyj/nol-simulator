/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nol.simulator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Ron
 */
public class FXMLSimulatorController implements Initializable {

    @FXML
    private Pane simulatorPane;
    @FXML
    private Button sleepBtn;
    @FXML
    private Button eatBtn;
    @FXML
    private Button entertainmentBtn;
    @FXML
    private Button coffeeBtn;
    @FXML
    private Button studyBtn;
    @FXML
    private Button dropOutBtn;
    @FXML
    private Button gymBtn;
    @FXML
    private Button puzzleBtn;
    @FXML
    private Button workBtn;
    @FXML
    private Button giveUpCareerBtn;
    @FXML
    private Button pay2ClickBtn;
    @FXML
    private Button buyCoffeeMBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void sleepAction(ActionEvent event) {
        if ((MainNoLSim.credits < 10) || (MainNoLSim.hunger == 0)) {

        } else {
            MainNoLSim.setCredit(-10);
            MainNoLSim.setEnergy(+50);
            MainNoLSim.setHunger(-5);
        }
    }

    @FXML
    private void eatAction(ActionEvent event) {
        if ((MainNoLSim.credits < 15) || (MainNoLSim.energy == 0) || (MainNoLSim.mood == 0)) {

        } else {
            MainNoLSim.setCredit(-15);
            MainNoLSim.setEnergy(-2);
            MainNoLSim.setHunger(40);
            MainNoLSim.setMood(-1);
        }
    }

    @FXML
    private void entertainmentAction(ActionEvent event) {
        if ((MainNoLSim.credits < 8) || (MainNoLSim.hunger == 0) || (MainNoLSim.energy == 0)) {

        } else {
            MainNoLSim.setCredit(-8);
            MainNoLSim.setEnergy(-5);
            MainNoLSim.setHunger(-5);
            MainNoLSim.setMood(30);
        }
    }

    @FXML
    private void coffeeAction(ActionEvent event) {
        if (MainNoLSim.credits < 20) {
            MainNoLSim.setCredit(-20);
            MainNoLSim.setEnergy(25);
            MainNoLSim.setHunger(15);
            MainNoLSim.setMood(30);
        }
    }

    @FXML
    private void studyAction(ActionEvent event) {
        if ((MainNoLSim.energy == 0) || (MainNoLSim.hunger == 0) || (MainNoLSim.mood == 0)) {
            MainNoLSim.studyFlag = false;
        } else {
            MainNoLSim.studyFlag = true;
            MainNoLSim.setCredit(-3);
            MainNoLSim.setEnergy(-5);
            MainNoLSim.setHunger(-10);
            MainNoLSim.setMood(-15);
            dropOutBtn.setDisable(false);
            gymBtn.setDisable(false);
        }
    }

    @FXML
    private void dropOutAction(ActionEvent event) {
        MainNoLSim.studyFlag = false;
        dropOutBtn.setDisable(true);
        gymBtn.setDisable(true);
    }

    @FXML
    private void gymAction(ActionEvent event) {
        if ((MainNoLSim.energy == 0) || (MainNoLSim.hunger == 0) || (MainNoLSim.mood == 0)) {

        } else {
            MainNoLSim.setEnergy(-10);
            MainNoLSim.setHunger(-5);
            MainNoLSim.setMood(2);
        }
    }

    @FXML
    private void workAction(ActionEvent event) {
        if ((MainNoLSim.energy == 0) || (MainNoLSim.hunger == 0) || (MainNoLSim.mood == 0)) {
            MainNoLSim.workingFlag = false;
        } else {
            MainNoLSim.workingFlag = true;
            MainNoLSim.setCredit(90);
            giveUpCareerBtn.setDisable(false);
            MainNoLSim.setEnergy(-10);
            MainNoLSim.setHunger(-5);
            MainNoLSim.setMood(-10);
        }
    }

    @FXML
    private void giveUpCarreerAction(ActionEvent event) {
        MainNoLSim.workingFlag = true;
        giveUpCareerBtn.setDisable(true);
    }

    @FXML
    private void puzzleAction(ActionEvent event) throws IOException {
        MainNoLSim.controller.simulatorPane.getChildren().clear();
        MainNoLSim.controller.simulatorPane.getChildren().add(MainNoLSim.puzzlePane);
    }

    @FXML
    private void pay2ClickAction(ActionEvent event) {
        MainNoLSim.controller.simulatorPane.getChildren().clear();
        MainNoLSim.controller.simulatorPane.getChildren().add(MainNoLSim.urlPane);
    }

    @FXML
    private void buyCoffeeMAction(ActionEvent event) {
        if (MainNoLSim.credits > 2500) {
            MainNoLSim.setCredit(-2500);
            buyCoffeeMBtn.setDisable(true);
            coffeeBtn.setDisable(false);
        }
    }

}
