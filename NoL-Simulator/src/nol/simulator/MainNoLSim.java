/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nol.simulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import puzzle2048.MainPuzzle;
import url.shortener.MainURL;

/**
 *
 * @author Ron
 */
public class MainNoLSim extends Application {

    FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLNoLSim.fxml"));
    static FXMLNoLSimController controller;
    static MainPuzzle mainPz;
    static MainURL mainURL;
    static MainNoLSim mainNoLSim;
    static Parent nolSimPane;
    static Parent puzzlePane;
    static Parent urlPane;
    public static int dayCount = 1;
    public static int credits = 200;
    public static int energy = 100;
    public static int hunger = 100;
    public static int mood = 100;
    static File file;
    static ExecutorService exec = Executors.newCachedThreadPool();
    static DayCount dayCounting = new DayCount();
    static boolean studyFlag = false;
    static boolean workingFlag = false;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(initialize());
        stage.setResizable(false);
        stage.setTitle("NoL Life Simulator");
        stage.show();

        controller.creditsCount.setText(String.valueOf(credits));
        controller.setDayCount(dayCount);
        setEnergy(0);
        setHunger(0);
        setMood(0);
        controller.backAction(null);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        launch(args);

        beforeExit();
        mainURL.beforeExit();
        mainPz.beforeExit();
    }

    public Scene initialize() throws IOException {
        Parent root = loader.load();
        controller = loader.getController();

        Scene scene = new Scene(root);
        scene.setOnKeyPressed(ke -> {
            KeyCode keyCode = ke.getCode();
            if (keyCode.isArrowKey()) {
                mainPz.gridControl.actionKey(keyCode);
            }
        });

        mainPz = new MainPuzzle();
        mainNoLSim = new MainNoLSim();
        mainURL = new MainURL();
        try {
            nolSimPane = MainNoLSim.mainNoLSim.simInitialize();
            puzzlePane = MainNoLSim.mainPz.initialize();
            urlPane = MainNoLSim.mainURL.initialize();
        } catch (IOException e) {
        }

        file = new File("NoLSimulatorData.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        String readLine = null;
        String[] strLine = null;
        BufferedReader fileReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
        while ((readLine = fileReader.readLine()) != null) {
            strLine = readLine.split("[;]");
            dayCount = Integer.valueOf(strLine[0]);
            credits = Integer.valueOf(strLine[1]);
            energy = Integer.valueOf(strLine[2]);
            hunger = Integer.valueOf(strLine[3]);
            mood = Integer.valueOf(strLine[4]);
        }

        exec.execute(dayCounting);
        return scene;
    }

    public static void beforeExit() throws IOException {
        dayCounting.interrupt();
        exec.shutdown();

        try (FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), false)) {
            fileWriter.write(dayCount + ";" + credits + ";" + energy + ";" + hunger + ";" + mood);
            fileWriter.flush();
        }
    }

    public Parent simInitialize() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLSimulator.fxml"));
        return root;
    }

    public static void setEnergy(int value) {
        double update = 0;
        energy = energy + value;
        if (energy >= 100) {
            energy = 100;
            update = (double) 1;
        } else if (energy <= 0) {
            energy = 0;
            update = 0;
        } else {
            update = Double.valueOf("0." + String.valueOf(energy / 10));
        }

        controller.energyBar.setProgress(update);
        controller.energyValue.setText(String.valueOf(energy));
    }

    public static void setHunger(int value) {
        double update = 0;
        hunger = hunger + value;
        if (hunger >= 100) {
            hunger = 100;
            update = (double) 1;
        } else if (hunger <= 0) {
            hunger = 0;
            update = 0;
        } else {
            update = Double.valueOf("0." + String.valueOf(hunger / 10));
        }

        controller.hungerBar.setProgress(update);
        controller.hungerValue.setText(String.valueOf(hunger));
    }

    public static void setMood(int value) {
        double update = 0;
        mood = mood + value;
        if (mood >= 100) {
            mood = 100;
            update = (double) 1;
        } else if (mood <= 0) {
            mood = 0;
            update = 0;
        } else {
            update = Double.valueOf("0." + String.valueOf(mood / 10));
        }

        controller.moodBar.setProgress(update);
        controller.moodValue.setText(String.valueOf(mood));
    }

    public static void setCredit(int value) {
        credits = credits + value;
        controller.creditsCount.setText(String.valueOf(credits));
    }

    public static class DayCount implements Runnable {

        public boolean isInterrupt = false;
        public volatile boolean dayCountFlag = false;

        public void run() {
            while (!isInterrupt) {
                if (dayCountFlag) {
                    try {
                        synchronized (this) {
                            this.wait(1000);
                        }
                        dayCount++;
                        //controller.setDayCount(dayCount);
                        System.out.println("DayCount : " + dayCount);
//                        if (studyFlag) {
//                            MainNoLSim.credits = MainNoLSim.credits +  3;
//                        }
//                        if (workingFlag) {
//                            MainNoLSim.credits += 90;
//                        }
                    } catch (InterruptedException ex) {
                    }
                } else {
                }
            }
        }

        public void interrupt() {
            isInterrupt = true;
        }

        public void toggleDayCount(boolean flag) {
            dayCountFlag = flag;
        }
    }

}
