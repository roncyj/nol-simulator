# NoL Simulator#
* NoL Simulator is a Life Simulator that allow you to make decisions to develop a virtual in-game life.
* Alpha version

* P.S. - application jar files is stored in the path "NoL-Simulator\applicationJar"

### In-Game Features ###

* 2048 Puzzle Competition
* URL Shortener & Pay2Click
* Simulating typical Real World life cycle (Study in University -> Work)

### Updates in Future ###

* Multi User
* Progress features for University and Work
* More item to sell in shop
* Quest and Objective to earn more in-game credits
* More Mini-Games
* Create course in University
* Create more job opportunity

### Known Error/Bug ###
* Day Counter
	* able to count Day on console but not able to display on application
	* features that are related to Day are yet functional

### Developer ###

* RonCYJ